# TP2 : Déploiement automatisé

<!-- vim-markdown-toc GitLab -->

* [I. Déploiement simple](#i-déploiement-simple)
* [II. Re-package](#ii-re-package)
* [III. Multi-node deployment](#iii-multi-node-deployment)
* [IV. Automation here we (slowly) come](#iv-automation-here-we-slowly-come)

<!-- vim-markdown-toc -->

# I. Déploiement simple

Voir [section 1](./part1).

# II. Re-package

Voir [section 2](./part2).

```bash
[it4@nowhere scripts]$ vagrant up
[...]

[it4@nowhere scripts]$ vagrant package --output b2-tp2-centos.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: /home/it4/ThrashDir/ynov/2020-2021/b2-linux/b2-linux-2020/tp/2/suggestion_correction/part2/scripts/b2-tp2-centos.box

[it4@nowhere scripts]$ vagrant box add b2-tp2-centos.box --name b2-tp2-centos
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'b2-tp2-centos' (v0) for provider: 
    box: Unpacking necessary files from: file:///home/it4/ThrashDir/ynov/2020-2021/b2-linux/b2-linux-2020/tp/2/suggestion_correction/part2/scripts/b2-tp2-centos.box
==> box: Successfully added box 'b2-tp2-centos' (v0) for 'virtualbox'!

```

# III. Multi-node deployment

Voir [Section 3](./part3).

# IV. Automation here we (slowly) come

Voir [section 4](./part4).

**Il est nécessaire de modifier l'adresse du Webhook Discord afin que vous receviez l'adresse dans VOTRE Discord**.  
La modification se fait tout en haut du [fichier `./part4/scripts/node1.sh`](./part4/scripts/node1.sh).

```bash
[it4@nowhere part4]$ vagrant up
[it4@nowhere part4]$ vagrant ssh node2

# NGINX websites
[vagrant@node2 ~]$ curl https://node1.tp1.b2/site1 -L
<h1>Hello from site 1</h1>
[vagrant@node2 ~]$ curl https://node1.tp1.b2/site2 -L
<h1>Hello from site 2</h1>

# Netdata
[vagrant@node2 ~]$ curl http://node1.tp1.b2:19999 -L
[...]
```

Test des alertes Netdata (issu de [la doc officielle](https://learn.netdata.cloud/docs/agent/health/notifications#testing-notifications)) :
```bash
# become user netdata
[vagrant@node1 ~]$ sudo su -s /bin/bash netdata

bash-4.2$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.2$ /usr/libexec/netdata/plugins.d/alarm-notify.sh test
```
