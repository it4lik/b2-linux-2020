| Etudiant       | Part | Réseau | SSH + User | Firewall + SELinux | Serv Web | HTTPS | Backup | Cron | Restauration | Monitoring | Note |
|----------------|------|--------|------------|--------------------|----------|-------|--------|------|--------------|------------|------|
| abdoul-wahab   | x    | x      | x          | x                  | x        | -     | o      | x    | o            | -          | 11   |
| adone          | x    | x      | x          | o                  | x        | o     | -      | x    | o            | -          | 12   |
| auriol         | x    | x      | x          | x                  | x        | x     | x      | x    | o            | x          | 17   |
| BERGEZ-CASALOU | x    | x      | -          | -                  | x        | o     | -      | o    | o            | x          | 12   |
| boileau        | x    | o      | o          | o                  | x        | o     | -      | x    | o            | -          | 10   |
| bonnin         | x    | x      | x          | x                  | x        | o     | -      | o    | o            | -          | 11   |
| bordrez        | x    | x      | x          | x                  | x        | x     | o      | x    | -            | +          | 13   |
| boudeau        | x    | x      | x          | -                  | x        | x     | o      | o    | o            | o          | 9    |
| buton          | o    | x      | x          | o                  | -        | o     | o      | o    | o            | o          | 5    |
| chancerel      | x    | x      | x          | x                  | x        | x     | o      | o    | o            | x          | 12   |
| christophe     | x    | x      | x          | x                  | x        | o     | o      | o    | o            | o          | 8    |
| crenn          | x    | x      | x          | x                  | x        | x     | x      | x    | o            | x          | 17   |
| DARRIBAU       | x    | x      | x          | x                  | x        | x     | o      | o    | o            | o          | 10   |
| Dayot          | x    | x      | x          | -                  | x        | x     | x      | x    | -            | x          | 18   |
| Dournet        | x    | x      | x          | x                  | x        | x     | -      | x    | o            | x          | 15   |
| dugoua         | x    | x      | x          | -                  | x        | o     | x      | o    | o            | o          | 12   |
| dupuis         | -    | x      | x          | -                  | x        | x     | x      | x    | o            | -          | 14   |
| duvigneau      | x    | x      | x          | -                  | x        | -     | x      | x    | o            | x          | 16   |
| ezzahi         | x    | x      | x          | -                  | x        | o     | x      | o    | o            | -          | 13   |
| ferreirasilva  | x    | x      | x          | o                  | x        | x     | -      | x    | o            | x          | 13   |
| garcia         | x    | x      | x          | -                  | x        | x     | -      | o    | -            | x          | 14   |
| ghouthi        | o    | o      | o          | o                  | o        | o     | o      | o    | o            | o          | 0    |
| guiheneuc      | x    | -      | x          | -                  | x        | o     | o      | o    | o            | o          | 9    |
| judeaux        | x    | x      | x          | x                  | -        | o     | -      | o    | o            | o          | 8    |
| labedade       | x    | x      | x          | -                  | -        | -     | x      | x    | o            | o          | 11   |
| lachamp        | x    | x      | x          | -                  | x        | x     | -      | x    | o            | x          | 14   |
| laforest       | o    | o      | o          | o                  | o        | o     | o      | o    | o            | o          | 0    |
| lautier        | x    | x      | x          | -                  | o        | o     | -      | o    | o            | o          | 6    |
| lefebvre       | x    | x      | x          | -                  | x        | x     | x      | x    | -            | x          | 18   |
| marechal       | x    | x      | x          | -                  | x        | x     | -      | x    | o            | -          | 15   |
| megy           | x    | x      | -          | o                  | -        | o     | x      | x    | o            | x          | 14   |
| michon         | x    | x      | x          | -                  | x        | x     | x      | x    | o            | -          | 16   |
| mickhail       | x    | x      | x          | -                  | x        | x     | x      | x    | -            | o          | 16   |


| x              | 1    | 1      | 1          | 1                  | 4        | 2     | 4      | 1    | 3            | 2          | 20   |
