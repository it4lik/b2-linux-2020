# Mémos

* [Mémo des commandes et fichiers élémentaires du système](./basics.md)
* [Mémo partitionnement, LVM](./part.md)
* [Mémo réseau CentOS 7](./centos_network.md)
