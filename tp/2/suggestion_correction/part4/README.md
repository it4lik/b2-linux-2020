# Section 4

Pour que le monitoring soit fonctionnel, en particulier les alertes Discord, il sera nécessaire de modifier une ligne dans l'un des scripts de déploiement. 

Il faut modifier la variable `DISCORD_WEBHOOK_URL` définie tout en haut du script `./scripts/node1.sh` qui renseigne l'URL de votre Webhook Discord. 

> Pour obtenir votre adresse de Webhook pour votre serveur Discord, RDV dans [la section dédiée sur la doc officielle de Discord](https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks).
