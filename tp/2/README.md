# TP2 : Déploiement automatisé

Le but de ce TP est d'effectuer le même déploiement que lors du [TP1](../1/) mais en automatisant le déploiement de la machine virtuelle, sa configuration basique, ainsi que l'install et la conf des services.

Au menu :
* réutilisation du [TP1](../1/)
* utilisation de [Vagrant](https://www.vagrantup.com/)
* premiers pas dans l'automatisation

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
    * [Install Vagrant](#install-vagrant)
    * [Init Vagrant](#init-vagrant)
* [I. Déploiement simple](#i-déploiement-simple)
* [II. Re-package](#ii-re-package)
* [III. Multi-node deployment](#iii-multi-node-deployment)
* [IV. Automation here we (slowly) come](#iv-automation-here-we-slowly-come)

<!-- vim-markdown-toc -->

# 0. Prérequis

## Install Vagrant

Téléchargez [Vagrant](https://www.vagrantup.com/) depuis le site officiel. Une fois téléchargé, assurez-vous que vous avez la commande `vagrant` dans votre terminal.

Vous aurez aussi besoin de VirtualBox. **Je n'apporterai aucun support si vous utilisez un autre hyperviseur.**

Vagrant est un outil qui sert de surcouche à un hyperviseur ; dans notre cas, il pilotera VirtualBox. 

Le fonctionnement de Vagrant est simple :
* on décrit une ou plusieurs VM(s) dans un fichier appelé `Vagrantfile`
* on demande à Vagrant d'allumer la ou les VM(s)

La description des VMs se fait dans un langage spécifique, dérivé de Ruby.

## Init Vagrant

```bash
# Créez vous un répertoire de travail
$ mkdir vagrant
$ cd vagrant

# Initialisez un Vagrantfile
$ vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

> Je vous invite à **lire** le fichier Vagrantfile qui a été généré automatiquement pour voir une partie de ce que Vagrant est capable de réaliser.

Une fois le Vagrantfile généré, épurez-le en enlevant les commentaires, et ajoutez des lignes afin qu'il ressemble à ça :

```ruby
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  ## Les 3 lignes suivantes permettent d'éviter certains bugs et/ou d'accélérer le déploiement. Gardez-les tout le temps sauf contre-indications.
  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false
  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 
  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true
end
```

> Si vous avez l'erreur `Unknown configuration section 'vbguest'`, lancez la commande `vagrant plugin install vagrant-vbguest` AVANT le `vagrant up`.

Test du bon fonctionnement :
```bash
# Toujours dans le dossier où a été généré le Vagrantfile
$ vagrant up
[...]

# Voir l'état de la machine
$ vagrant status
# Vous pouvez aussi jeter un oeil dans votre VirtualBox : une VM devrait avoir pop

# Se connecter à la machine
$ vagrant ssh

# Détruire la VM et les fichiers associés
$ vagrant destroy -f
```

# I. Déploiement simple

🌞 Créer un `Vagrantfile` qui :
* utilise la box `centos/7`
* crée une seule VM
  * 1Go RAM
  * ajout d'une IP statique `192.168.2.11/24`
  * définition d'un nom (interne à Vagrant)
  * définition d'un hostname 

🌞 Modifier le `Vagrantfile`
* la machine exécute un script shell au démarrage qui installe le paquet `vim`
* ajout d'un deuxième disque de 5Go à la VM

> **IMPORTANT :** je vous recommande fortement d'ajouter un fichier `.gitignore` à la racine de votre dépôt git afin d'ignorer ce nouveau disque lorsque vous effectuez des `git push`.

Pour exécuter un script shell au démarrage, la syntaxe recommandée est :
```ruby
# Exécution d'un script au démarrage de la VM
config.vm.provision "shell", path: "script.sh"
```

# II. Re-package

Il est possible de packager soi-même une *box* Vagrant afin d'avoir une VM sur-mesure dès qu'elle s'allume. 

On peut la créer depuis le fichier `.iso` correspondant à l'image officielle d'un OS donné.  
Il est aussi possible de la générer à partir d'une *box* existante, c'est ce que nous allons faire ici.

La démarche est la suivante :
* on allume une VM de base
* à l'intérieur de la VM, on effectue les modifications souhaitées
  * création de fichiers
  * ajout de paquets
  * config système
  * etc.
* on exit la VM, en la gardant allumée
* utilisation d'une commande `vagrant` pour créer une nouvelle box à partir de la VM existante

En CLI, ça donne :

```bash
# Allumage de la  VM
$ vagrant up

# Connexion dans la VM + modifications souhaitées
$ vagrant ssh
[...]

# On se déconnecte de la VM, et on repackage
$ exit
$ vagrant package --output centos7-custom.box
$ vagrant box add centos7-custom centos7-custom.box
```

Repackager une box, **que vous appelerez `b2-tp2-centos`** en partant de la box `centos/7`, qui comprend (toutes ces actions doivent être exécutées *via* un script au démarrage de la VM) :
* une mise à jour système
  * `yum update`
* l'installation de paquets additionels
  * `vim`
  * `epel-release`
  * `nginx`
* désactivation de SELinux
* firewall (avec `firewalld`, en utilisant la commande `firewall-cmd`)
  * activé au boot de la VM
  * ne laisse passser que le strict nécessaire (SSH)

> **IMPORTANT :** si vous laissez le fichier `.box` dans le même dossier que votre dépôt git, ajoutez le au `.gitignore` pour ne pas pousser la box en ligne.

# III. Multi-node deployment

Il est possible de déployer et gérer plusieurs VMs en un seul `Vagrantfile`.

Exemple :
```ruby
Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
  config.vm.box = "centos/7"

  # Config une première VM "node1"
  config.vm.define "node1" do |node1|
    # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
    node1.vm.network "private_network", ip: "192.168.56.11"
  end

  # Config une première VM "node2"
  config.vm.define "node2" do |node2|
    # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
    node2.vm.network "private_network", ip: "192.168.56.12"
  end
end
```

🌞 Créer un `Vagrantfile` qui lance deux machines virtuelles, **les VMs DOIVENT utiliser votre box repackagée comme base** :

| x           | `node1.tp2.b2` | `node2.tp2.b2` |
|-------------|----------------|----------------|
| IP locale   | `192.168.2.21` | `192.168.2.22` |
| Hostname    | `node1.tp2.b2` | `node2.tp2.b2` |
| Nom Vagrant | `node1`        | `node2`        |
| RAM         | 1Go            | 512Mo          |

# IV. Automation here we (slowly) come

Cette dernière étape vise à automatiser la résolution du TP1 à l'aide de Vagrant et d'un peu de scripting.

**Le but :**
* remettre en place le TP1
  * une VM serveur Web
  * une VM cliente
* les confs doivent être identiques au TP1
  * sauf pour le partitionnement, je vous l'épargne
  * TOUT le reste doit y figurer
  * les actions seront réalisées à l'aide d'un script qui se lance au démarrage de la VM
* **en plus** de ce qui a été fait dans le TP1, le client doit trust le certificat du serveur
  * c'est à dire que vous n'avez pas besoin d'ajouter le `-k` de à `curl` pour que vos requêtes HTTPS passent
  * il faut utiliser le répertoire `/usr/share/pki/ca-trust-source/`

🌞 Créer un `Vagrantfile` qui automatise la résolution du TP1
* pour corriger, je vais moi aussi créer la box `b2-tp2-centos` sur ma machine, comme vous
* si tout se passe bien, pour tester que tout est fonctionnel, j'ai juste besoin de :

```bash
$ vagrant up
$ vagrant ssh node2
$ curl -L https://node1.tp2.b2
```
