#!/bin/bash
# it4

# Network
echo 'Setup networking'
echo '192.168.30.11 gitea-node' >> /etc/hosts
echo '192.168.30.12 db-node' >> /etc/hosts
echo '192.168.30.13 nginx-node' >> /etc/hosts
echo '192.168.30.14 nfs-node' >> /etc/hosts
