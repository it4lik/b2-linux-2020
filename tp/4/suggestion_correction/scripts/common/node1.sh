#!/bin/bash
# it4
# 05/10/2020

# URL of a webhook on your Discord Server
DISCORD_WEBHOOK_URL='https://discord.com/api/webhooks/765549391695446067/2-A1Dke4ZSd9ubh_ZyibDAGc4LzeB3j0PnUziXOgbe6FJ2FiIpWqUrnjhQWNi8MPPYrq'

# Network
echo 'Setup networking'
echo '192.168.56.11 node1 node1.tp1.b2' >> /etc/hosts
echo '192.168.56.12 node2 node2.tp1.b2' >> /etc/hosts

# SELinux
echo 'Disabling SELinux'
setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config

# Users
echo 'Add admin user'
useradd admin -m
usermod -aG wheel admin
echo 'toto' | passwd vagrant --stdin

# Setup web server with NGINX
echo 'Install + configuration of NGINX'
## Install package
yum install -y epel-release
yum install -y nginx

## Create applicative dedicated user
useradd web -M -s /sbin/nologin

## Place key in the correct path, and define restrictive permissions
mv /tmp/node1.tp1.b2.key /etc/pki/tls/private/node1.tp1.b2.key
chmod 400 /etc/pki/tls/private/node1.tp1.b2.key
chown web:web /etc/pki/tls/private/node1.tp1.b2.key

## Place cert in the correct path, and define restrictive permissions
mv /tmp/node1.tp1.b2.crt /etc/pki/tls/certs/node1.tp1.b2.crt
chown web:web /etc/pki/tls/certs/node1.tp1.b2.crt
chmod 444 /etc/pki/tls/certs/node1.tp1.b2.crt

## Create sample websites
echo '<h1>Hello from site 1</h1>' | tee /srv/site1/index.html
echo '<h1>Hello from site 2</h1>' | tee /srv/site2/index.html
chown -R web:web /srv/site1 /srv/site2
chmod 740 /srv/site1 /srv/site2
chmod 440 /srv/site1/index.html /srv/site2/index.html

## Copy nginx.conf file (it's dropped in /tmp by the Vagrantfile)
mv /tmp/nginx.conf /etc/nginx/nginx.conf

## Start NGINX
systemctl enable --now nginx

## Open NGINX-related ports
firewall-cmd --add-port 80/tcp
firewall-cmd --add-port 80/tcp --permanent
firewall-cmd --add-port 443/tcp
firewall-cmd --add-port 443/tcp --permanent

# Setup the backup script
echo 'Setting up the backup script'
## Create dedicated user
useradd backup -M -s /sbin/nologin
usermod -aG web backup

## Move the script in the correct place
mv /tmp/tp1_backup.sh /opt/tp1_backup.sh

## Create backups destination dir
mkdir /opt/backup
chown -R backup:backup /opt/backup
chmod 700 /opt/backup

## Setup permissions (execution) on the backup script
chmod 700 /opt/tp1_backup.sh

## Configure cron to run periodically our backup script
echo '# Sauvegarde de /srv/site1 toutes les heures' >> /etc/crontab
echo '30 * * * * backup /opt/tp1_backup.sh /srv/site1' >> /etc/crontab
echo '# Sauvegarde de /srv/site2 toutes les heures' >> /etc/crontab
echo '30 * * * * backup /opt/tp1_backup.sh /srv/site2' >> /etc/crontab

## Move systemd-related files into the correct directory
mv /tmp/backup.service /etc/systemd/system/backup.service
mv /tmp/backup.timer /etc/systemd/system/backup.timer
systemctl daemon-reload
systemctl enable --now backup.timer

# Netdata
echo 'Install Netdata'
## Get the install script
curl -Ss https://my-netdata.io/kickstart.sh -o /tmp/kickstart.sh
chmod +x /tmp/kickstart.sh
# Le --dont-wait permet une installation non interactive
# J'ai trouvé ça là : https://github.com/netdata/netdata/issues/2305 :)
/tmp/kickstart.sh --dont-wait

# We move a default config file
mv /tmp/health_alarm_notify.conf /etc/netdata/health_alarm_notify.conf
# Edit config file with our specific Discord webhook URL
sed -i "s|DISCORD_WEBHOOK_URL=\"\"|DISCORD_WEBHOOK_URL=\"${DISCORD_WEBHOOK_URL}\"|" /etc/netdata/health_alarm_notify.conf
sed -i "s|DEFAULT_RECIPIENT_DISCORD=\"\"|DEFAULT_RECIPIENT_DISCORD=\"sysadmin\"|" /etc/netdata/health_alarm_notify.conf

# Start and enable Netdata
systemctl enable --now netdata

## Open netdata web port
firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent

