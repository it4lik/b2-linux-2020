# TP

* [TP1 : Déploiement classique](./1/README.md)
* [TP2 : Déploiement automatisé](./2/README.md)
* [TP3 : systemd](./3/README.md)
* [TP4 : Déploiement multi-noeud](./4/README.md)

## Rendu de TP 

L'OS GNU/Linux utilisé pour les TPs sera CentOS7, sauf contre-indication.

### Format de rendu 

Le rendu de TP **DOIT** se faire *via* un dépôt git, au format Markdown.  
Votre dépôt git **DOIT** être léger (c'est à dire qu'il ne doit par exemple par contenir de disque en `.vdi` ou de box Vagrant `.box`). Pensez au `.gitignore`.

TOUTES les opérations pour réaliser ce qui est demandé doivent figurer dans le rendu de TP
* au format Markdown
* ou dans un script (Vagrantfile, scripts shell, etc.)

Les points précédés d'un 🌞 doivent **obligatoirement** figurer dans le rendu.  
Les points précédés d'un 🐙 sont des bonus, ils sont facultatifs.

### Modalités de rendu

Le rendu de TP se fait *via* Discord, en m'envoyant un message privé au format `nom,url_du_dépôt_git`.  
Par exemple : `macron,https://gitlab.inria.fr/stopcovid19/accueil`.

**Un rendu par TP et par personne.**

**NOTE :** à partir du TP2, vous utiliserez **systématiquement** Vagrant pour créer vos environnement virtuels. Le(s) `Vagrantfile`(s) **devront** être livrés dans le dépôt de rendu.  
Vous trouverez une section dédiée concernant la structure du dépôt attendue à la fin de chaque TP.
