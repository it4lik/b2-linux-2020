#!/bin/bash

# Disable SELinux
setenforce 0
sed -i 's/enforcing/permissive/' /etc/selinux/config

# Update system and repos
yum update -y

# Install and configure firewalld
yum install -y firewalld
systemctl enable --now firewalld
firewall-cmd --add-port=22/tcp --permanent
firewall-cmd --add-port=22/tcp

# Install other packages
yum install -y vim git

# Configure an admin user
echo 'Add admin user'
useradd admin -m
usermod -aG wheel admin
echo 'toto' | passwd admin --stdin

# Setup the backup script
echo 'Setting up the backup script'
## Create dedicated user
useradd backup -M -s /sbin/nologin -u 3000
usermod -aG web backup

## Move the script in the correct place
mv /tmp/tp1_backup.sh /opt/tp1_backup.sh

## Create backups destination dir
mkdir /opt/backup
chown -R backup:backup /opt/backup /opt/tp1_backup.sh
chmod 700 /opt/backup

## Setup permissions (execution) on the backup script
chmod 700 /opt/tp1_backup.sh

## Configure cron to run periodically our backup script
echo '# Sauvegarde de /srv/site1 toutes les heures' >> /etc/crontab
echo '30 * * * * backup /opt/tp1_backup.sh /srv/site1' >> /etc/crontab
echo '# Sauvegarde de /srv/site2 toutes les heures' >> /etc/crontab
echo '30 * * * * backup /opt/tp1_backup.sh /srv/site2' >> /etc/crontab

## Move systemd-related files into the correct directory
mv /tmp/backup.service /etc/systemd/system/backup.service
mv /tmp/backup.timer /etc/systemd/system/backup.timer
systemctl daemon-reload
systemctl enable --now backup.timer

# Netdata
echo 'Install Netdata'
## Get the install script
curl -Ss https://my-netdata.io/kickstart.sh -o /tmp/kickstart.sh
chmod +x /tmp/kickstart.sh
# Le --dont-wait permet une installation non interactive
# J'ai trouvé ça là : https://github.com/netdata/netdata/issues/2305 :)
/tmp/kickstart.sh --dont-wait

# We move a default config file
mv /tmp/health_alarm_notify.conf /etc/netdata/health_alarm_notify.conf
# Edit config file with our specific Discord webhook URL
sed -i "s|DISCORD_WEBHOOK_URL=\"\"|DISCORD_WEBHOOK_URL=\"${DISCORD_WEBHOOK_URL}\"|" /etc/netdata/health_alarm_notify.conf
sed -i "s|DEFAULT_RECIPIENT_DISCORD=\"\"|DEFAULT_RECIPIENT_DISCORD=\"sysadmin\"|" /etc/netdata/health_alarm_notify.conf

# Start and enable Netdata
systemctl enable --now netdata

## Open netdata web port
firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent
