# TP4 : Déploiement multi-noeud

On va continuer à avancer sur notre sujet de déploiement de services. Le but dans ce TP est de monter une stack de services proche de ce que l'on fait dans le monde réel :
* les services sont éclatés sur plusieurs machines
* plusieurs bonnes pratiques sont respectées
  * firewall
  * gestion d'utilisateurs et de permissions
* des services et outils additionels sont mis en place pour améliorer la sécurité de l'application
  * HTTPS
  * reverse proxy
  * sauvegarde
  * monitoring

---

<!-- vim-markdown-toc GitLab -->

* [0. Prerequisites](#0-prerequisites)
* [I. Consignes générales](#i-consignes-générales)
    * [Liste des hosts](#liste-des-hosts)
    * [Une base commune](#une-base-commune)
* [II. Présentation de la stack](#ii-présentation-de-la-stack)
    * [1. Gitea](#1-gitea)
    * [2. MariaDB](#2-mariadb)
    * [3. NGINX](#3-nginx)
    * [4. Serveur NFS](#4-serveur-nfs)
    * [5. Sauvegarde](#5-sauvegarde)
    * [6. Monitoring](#6-monitoring)
    * [7. Bonus](#7-bonus)
        * [Isolation IP](#isolation-ip)
        * [Vérification du binaire Gitea](#vérification-du-binaire-gitea)

<!-- vim-markdown-toc -->

## 0. Prerequisites

On part toujours d'une box `centos/7` comme base, libre à vous de la repackager. Si vous la repackager, je dois avoir le Vagrantfile original et les fichiers dont il dépend pour reconstituer votre box.

Choses intéressantes à faire dans la box repackagée (liste non exhaustive) :
* mise à jour du système
  * `yum update`
* installer des paquets
  * `vim`
  * autres
* désactiver SELinux

> Attention : n'ajouter cependant rien de superflu à l'intérieur. Par exemple, n'installez pas NGINX partout alors que ce n'est pas nécessaire. Cela alourdirait les machines qui ne s'en servent pas de façon inutile.

## I. Consignes générales

🌞 Pour ce TP vous devez me rendre un unique Vagrantfile qui monte tout le TP :
* le Vagrantfile crée une machine par service
* vous livrerez tous les scripts nécessaires au bon fonctionnement de la solution
* comme dans les TP2 et TP3, tout devra être configur& automatiquement au démarrage du Vagrantfile
* **cette partie n'est qu'une présentation. Vous trouverez une suite d'étapes dans le II.**

---

**Chaque service (Gitea, NGINX, etc.) sera hébergé sur sa propre machine** :
* meilleur niveau de sécurité
  * chaque machine est configurée sur mesure
  * uniquement le strict minimum
* meilleures performances
  * par exemple une base de données consomme beaucoup de RAM, un reverse proxy plutôt du CPU. Les machines pourront donc être dimensionnées en fonction

---

Essayez d'être propres :
* si les scripts à lancer quand les machines commencent à être longs, pensez à les éclater en plusieurs scripts
* organisez les fichiers liés au Vagrantfile pour facilement s'y retrouver
  * inspirez vous, par exemple, de [la suggestion de rendu pour la section 4 du TP2](https://gitlab.com/it4lik/b2-linux-2020/-/tree/master/tp/2/suggestion_correction/part4)
* n'oubliez pas le `.gitignore` pour éviter de pousser les fichiers comme les `.vdi` ou `.box`

Inspirez-vous des TPs précédents, vous avez déjà fait la plupart des choses qu'il faut mettre en place ici.  
Ou récupérez ce que j'ai fait en suggestion de correction ([la correction de la partie 4 du TP2](https://gitlab.com/it4lik/b2-linux-2020/-/tree/master/tp/2/suggestion_correction/part4) est plutôt complète) pour ce qui est de la backup ou encore de Netdata.

---

**Le but du TP : je fais `vagrant up` dans votre TP et la solution fonctionne direct :**
* tous les services sont montés (Gitea, NFS server, Base de données, Reverse proxy)
* les interfaces Web sont disponibles
  * Gitea
  * Monitoring des serveurs
* je peux créer des dépôts git dans l'interface web Gitea et les cloner en SSH
* une backup tourne une fois par jour
* les alertes remontent dans un discord que je choisis avant le `vagrant up`

Le rendu Markdown doit comporter :
* la liste des hosts
* la liste des interfaces Web joignables
* ce qu'il faut ajouter dans mon `/etc/hosts` si besoin est
* comment et quel fichier je dois modifier pour ajouter l'URL de mon discord pour les alertes
  * une variable dans un script ? :)

### Liste des hosts

Vous devrez me fournir dans le rendu Markdown qui accompagne le Vagrantfile un tableau qui répertorie chacune des machines, son IP, et son rôle. Quelque chose comme :

| Name               | IP   | Rôle       |
|--------------------|------|------------|
| `node1.some.thing` | <IP> | Serveur... |

### Une base commune

Toutes les machines déployées doivent :
* figurer dans le fichier `/etc/hosts` des autres
* porter un service 
* si le serveur comporte des données, il doit être sauvegardé (voir la section II.5. pour le détail de ce qui doit être backup)
* être monitorées
* posséder un firewall configuré pour ne laisser passer que le strict nécessaire

Je vous laisse réfléchir à ce qui est le mieux : qu'est-ce qui doit être dans la box repackagée, dans le Vagrantfile, ou dans un script qui est lancé au démarrage de la machine. N'hésitez pas à me solliciter pour en discuter.


## II. Présentation de la stack

Le but est de monter un ensemble de services permettant d'héberger des dépôts git. 

Chaque service doit être hébergé sur sa propre machine.

Pour cela, vous allez mettre en place :
* un [Gitea](https://github.com/go-gitea/gitea)
  * application libre et opensource (développée en Go) qui permet d'héberger des dépôts Git et fournit une interface Web
  * minimaliste (donc très léger, et performant, mais propose peu de fonctionnalités nativement
  * vous utiliserez la documentation officielle pour le mettre en place
* une base de données
  * vous utiliserez MariaDB (c'est un équivalent à MySQL sous les OS dérivés de RedHat comme CentOS)
  * la base de données sera utilisée par Gitea
  * effectuez quelques recherches pour essayer de renforcer un peu la sécurité de la base de données
* un NGINX
  * il fera office de reverse proxy : c'est à dire d'intermédiaire en le client et le serveur
    * cela permet d'effectuer du cache
    * ou encore de mettre en place de la sécurité comme un HTTPS
  * il devra être placé "en front" : on devra passer par lui pour accéder au Gitea
* un serveur NFS
  * un serveur NFS est un serveu de partage de fichiers sur le réseau
  * il permet à plusieurs machines d'accéder à un dossier d'une autre machine sur le réseau
  * il sera utilisé ici pour stocker les sauvegardes des autres machines

### 1. Gitea

Mettre en place Gitea en suivant la doc officielle. Vous utiliserez une installation "From binary" (cf [la doc](https://docs.gitea.io/en-us/install-from-binary/)). Faites en sorte de pouvoir lancer Gitea avec un `systemctl start gitea` (tout est dans la doc).

**NB** : pour le fichier `.service` que vous allez créer, vous **DEVEZ** l'épurer pour qu'il ne contienne que des lignes effectives (-> pas de commentaires) que **vous comprenez**.

> Vous n'êtes pas obligés d'effectuer la vérification avec la commande `gpg` fournie dans la doc. Si vous voulez fare une vérification d'intégrité (mp moi pour plus d'infos), faites le avec l'empreinte sha256 fournie par Gitea. Voir la section II.7. Bonus pour plus d'infos.

Une fois qu'il est possible d'accéder à l'interface Web, il vous sera demandé des identifiants de connexion à la base de données. On passe donc à l'install de la base de données.

### 2. MariaDB

Installer une base de données MariaDB depuis les dépôts officiels CentOS7.

Une fois mise en place, vous devrez donc la configurer en suivant [les instructions de la documentation Gitea](https://docs.gitea.io/en-us/database-prep/) pour que ce dernier puisse s'en servir.

Dernière étape : retourner sur l'interface Web de Gitea et compléter l'installation (n'oubliez pas de créer un utilisateur admin tout en bas).

### 3. NGINX

Installer NGINX et le configurer pour agir comme reverse proxy vers Gitea. Tout est dans la doc Gitea là encore :)

### 4. Serveur NFS

Configurer la dernière machine pour agir comme serveur NFS. Sur ce serveur vous créerez trois répertoires, qui seront respectivement dédiés aux trois précédents serveur.

Y'a pas mal de doc pour faire ça sur internet, comme toujours ciblez la doc pour CentOS7. Configurez bien votre firewall.

### 5. Sauvegarde

Vous devrez backup :
* le dossier dans `/etc` réservé à Gitea
  * il existe une commande dédiée dans Gitea pour backup la plupart des infos qui lui sont liées (pas obligatoire de l'utiliser)
* la base de données liée à Gitea
* la conf du serveur NGINX

> Toutes ces backups sont stockées sur le serveur NFS *via* un point de montage sur chacun des autres serveurs.

### 6. Monitoring

Netdata déployé partout avec des alertes qui remontent dans un Discord unique.

### 7. Bonus

#### Isolation IP

Faire en sorte que votre déploiement mette en place 3 réseaux :

| Nom         | Role du réseau                                            | Machines possédant une IP dans le réseau                                                      |
|-------------|-----------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Application | Communication privée entre les services                   | NGINX, Gitea, Base de données                                                                 |
| Backup      | Tous les flux de sauvegardes passent par ce réseau        | NGINX, Gitea, Base de données, Serveur NFS                                                    |
| Front       | Les clients utilisent ce réseau pour contacter le service | NGINX (accès Web pour visiter l'interface de Gitea), Gitea (accès SSH pour cloner les dépôts) |

#### Vérification du binaire Gitea

Lorsque vous récupérez le binaire `gitea`, il est préférable d'effectuer un contrôle d'intégrité. C'est à dire qu'on va s'assurer que le binaire `gitea` n'a pas été altéré lorsqu'on l'a récupéré (eg. téléchargement corrompu, ou vilain hacker qui vous a filé un autre fichier).

Pour le vérifier, préférez utiliser l'empreinte sha256. Marche à suivre :

```bash
# Récupérer le binaire gitea (dans la doc)
$ wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64

# Récupérer l'empreinte sha256 fournie par Gitea
$ wget -O gitea.sha256 https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64.sha256.sum

# Comparer l'empreinte du binaire téléchargé, et l'empreinte fournie par Gitea, si ce sont les mêmes le fichier n'a pas été altéré
## Empreinte fournie par gitea
$ cat gitea.sha256
## Empreinte calculée depuis le binaire
$ sha256sum gitea
```
