#!/bin/bash
# it4
# 05/10/2020
# Automate resolution of TP1 https://gitlab.com/it4lik/b2-linux-2020/-/tree/master/tp/1

# Network
echo 'Setup networking'
echo '192.168.56.11 node1 node1.tp1.b2' >> /etc/hosts
echo '192.168.56.12 node2 node2.tp1.b2' >> /etc/hosts

# SELinux
echo 'Disabling SELinux'
setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=enforcing/g' /etc/selinux/config

# Users
echo 'Add admin user'
useradd admin -m
usermod -aG wheel admin
echo 'toto' | passwd vagrant --stdin

## Place cert in the correct path, and make the system trust it
echo 'Trust node1.tp1.b2 cert'
mv /tmp/node1.tp1.b2.crt /etc/pki/ca-trust/source/anchors/node1.tp1.b2.crt
chmod 444 /etc/pki/ca-trust/source/anchors/node1.tp1.b2.crt
update-ca-trust
