# TP3 : systemd

Le but ici c'est d'explorer un peu systemd. 

systemd est un outil qui a été très largement adopté au sein des distributions GNU/Linux les plus répandues (Debian, RedHat, Arch, etc.). systemd occupe plusieurs fonctions :
* système d'init
* gestion de services
* embarque plusieurs applications très proche du noyau et nécessaires au bon fonctionnement du système
  * comme par exemple la gestion de la date et de l'heure, ou encore la gestion des périphériques
* PID 1

Ce TP3 a donc pour objectif d'explorer un peu ces différentes facettes. La finalité derrière tout ça est de vous faire un peu mieux appréhender comment marche un OS GNU/Linux ; mais aussi de façon plus générale vous faire mieux appréhender en quoi consiste l'application qu'on appelle "système d'exploitation" (car ui, c'est juste une application).

Au menu :
* manipulation des *unités systemd*, et en particulier les *services*
* analyse (succincte) du boot d'une machine GNU/Linux
* appréhension de certains des éléments embarqués avec systemd
  * tâche planifiées (alternative à cron)
  * gestion de l'heure
  * gestion des noms
* bonus frappe : on va réviser un peu la manipulation de la ligne de commande n_n
  * les lignes précédées d'un **|CLI|** font appel à vos talents sur la ligne de commande
  * en réponse à ces lignes, une seule ligne de commande est attendue

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
* [I. Services systemd](#i-services-systemd)
    * [1. Intro](#1-intro)
    * [2. Analyse d'un service](#2-analyse-dun-service)
    * [3. Création d'un service](#3-création-dun-service)
        * [A. Serveur web](#a-serveur-web)
        * [B. Sauvegarde](#b-sauvegarde)
* [II. Autres features](#ii-autres-features)
    * [1. Gestion de boot](#1-gestion-de-boot)
    * [2. Gestion de l'heure](#2-gestion-de-lheure)
    * [3. Gestion des noms et de la résolution de noms](#3-gestion-des-noms-et-de-la-résolution-de-noms)
* [Structure du dépôt attendu](#structure-du-dépôt-attendu)

<!-- vim-markdown-toc -->

# 0. Prérequis

> De toute évidence, vous utiliserez désormais Vagrant systématiquement pour créer votre environnement de travail.

Une VM suffira pour le TP. Je vous conseille d'utiliser une box `centos/7` comme base, et de la repackager avec : 
* une mise à jour complète du système (pas obligé si la connexion dont vous bénéficiez a deux de tension)
* NGINX installé
* d'autres trucs si vous le souhaitez (comme `vim` :D)
* désactivation de SELinux

**HA** et on va se reservir du script de backup du [TP1](../1/README.md).

# I. Services systemd

## 1. Intro

Section d'intro aux services systemd. Ui c'est ces trucs qu'on lance avec des commandes `systemctl start` par exemple.

Pour voir une liste de tous les services actuellement disponibles sur la machine, on peut interroger systemd :
```bash
# Liste les services actifs
$ sudo systemctl -t service

# Liste les services et leur état au boot
$ sudo systemctl list-unit-files -t service

# Liste tous les services
$ sudo systemctl list-unit-files -t service -a
```

🌞 Utilisez la ligne de commande pour sortir les infos suivantes :
* **|CLI|** afficher le nombre de *services systemd* dispos sur la machine
* **|CLI|** afficher le nombre de *services systemd* actifs et en cours d'exécution *("running")* sur la machine
* **|CLI|** afficher le nombre de *services systemd* qui ont échoué *("failed")* ou qui sont inactifs *("exited")* sur la machine
* **|CLI|** afficher la liste des *services systemd* qui démarrent automatiquement au boot *("enabled")*

---

**Okay mais un service c'est quoi ?**

Un service c'est juste un truc pratique pour lancer des processus ou des tâches simplement. Par "simplement", ça veut dire qu'une fois qu'on utilise une gestion de service, commes les *services systemd*, on a plus besoin de (entre autres) :
* connaître par coeur la commande pour lancer un truc
* connaître par coeur quelles applications doivent se lancer dans quel ordre pour que tout fonctionne
* gérer à la main l'environnement pour lancer une application
  * l'utilisateur qui lance l'app 
  * les droits qu'a l'application
  * etc.
* écrire des scripts shell inmaintenables pour maintenir tout ça n_n

**Donc concrètement, un service ça permet de lancer un processus ET gérer son environnement.**

## 2. Analyse d'un service

Pour voir le contenu d'un service existant :
```bash
# Affiche le path du fichier qui définit un service donné
$ systemctl status <SERVICE>

# Affiche le contenu de l'unité directement
$ systemctl cat <SERVICE>
```

**La ligne la plus importante est celle qui commence par `ExecStart=` :** c'est elle qui indique le binaire à exécuter quand le service est démarré (c'est à dire la commande à lancer pour que le service soit considéré comme "actif").

🌞 Etudiez le service `nginx.service`
* déterminer le path de l'unité `nginx.service`
* afficher son contenu et expliquer les lignes qui comportent :
  * `ExecStart`
  * `ExecStartPre`
  * `PIDFile`
  * `Type`
  * `ExecReload`
  * `Description`
  * `After`

> Les mans de systemd sont très complets : `man systemd.unit` et `man systemd.service` par exemple. Une recherche ggl ça marche aussi, la meilleure doc étant [la doc officielle](https://www.freedesktop.org/software/systemd/man/systemd.service.html) (PS : c'est la même chose que dans le `man` n_n)

🌞 **|CLI|** Listez tous les services qui contiennent la ligne `WantedBy=multi-user.target`

## 3. Création d'un service

Pour créer un service, il suffit de créer un fichier au bon endroit, avec une syntaxe particulière.

L'endroit qui est dédié à la création de services par l'administrateur est `/etc/systemd/system/`. Les services système (installés par des paquets par exemple) se place dans d'autres dossiers.

Une fois qu'un service a été ajouté, il est nécessaire de demander à systemd de relire tous les fichiers afin qu'il découvre le vôtre :
```bash
$ sudo systemctl daemon-reload
```

### A. Serveur web

🌞 Créez une unité de service qui lance un serveur web
* la commande pour lancer le serveur web est 
  * `python3 -m http.server <PORT>`
  * **OU** `python2 -m SimpleHTTPServer <PORT>`
* quand le service se lance, le port doit s'ouvrir juste avant dans le firewall
* quand le service se termine, le port doit se fermer juste après dans le firewall
* un utilisateur dédié doit lancer le service
* le service doit comporter une description
* le port utilisé doit être défini dans une variable d'environnement (avec la clause `Environment=`)

🌞 Lancer le service
* prouver qu'il est en cours de fonctionnement pour systemd
* faites en sorte que le service s'allume au démarrage de la machine
* prouver que le serveur web est bien fonctionnel

> N'oubliez pas de tester votre service : le lancer avec `systemctl start <SERVICE>` et vérifier que votre serveur web fonctionne avec un navigateur ou un `curl` par exemple.

### B. Sauvegarde

Ici on va réutiliser votre script de sauvegarde du [TP1](../1/README.md) que vous avez *bien évidemment* gardé.

🌞 Créez une unité de service qui déclenche une sauvegarde avec votre script
* le script doit se lancer sous l'identité d'un utilisateur dédié
* le service doit utiliser un PID file
* le service doit posséder une description
* vous éclaterez votre script en trois scripts :
  * un script qui se lance AVANT la sauvegarde, qui effectue les tests
  * script de sauvegarde
  * un script qui s'exécute APRES la sauvegarde, et qui effectue la rotation (ne garder que les 7 sauvegardes les plus récentes)
  * une fois fait, utilisez les clauses `ExecStartPre`, `ExecStart` et `ExecStartPost` pour les lancer au bon moment

---

Les fichiers `.timer` systemd permettent de démarrer des unités de façon périodique. On peut voir les timers en cours d'exécution avec :
```bash
$ systemctl list-timers
```

Pour qu'un timer soit effectif, il doit porter le même nom que le service, mais suffixé par `.timer`. Dans le cas d'un `backup.service`, le nom du timer associé sera donc `backup.timer`.  
Pour activer un timer :
```bash
# Stopper le service s'il existe djéà et s'il s'exécute au boot
$ sudo systemctl stop <SERVICE>
# Démarrage du timer
$ sudo systemctl start <TIMER>
$ sudo systemctl enable <TIMER>
```


🌞 Ecrire un fichier `.timer` systemd
* lance la backup toutes les heues

🐙 Améliorer la sécurité du service de sauvegarde

```bash
# Commande permettant de mettre en évidence des faiblesses de sécurité au sein d'un service donné
$ systemd-analyze security <SERVICE>
```
* **NB** : la version de systemd livré avec CentOS 7 est trop vieille pour cette feature, il vous CentOS 8 (ou un autre OS récent). **Il faudra faire une mise à jour système pour profiter de cette feature**.
* mettre en place des mesures de sécurité pour avoir un score inférieur à 7

# II. Autres features

**Pour cette section, il sera nécessaire d'utiliser une version plus récente de systemd**. Vous devrez donc changer de box Vagrant, et utiliser une box possédant une version plus récente (par exemple une box CentOS8 ou une box Fedora récente).

---

## 1. Gestion de boot

🌞 Utilisez `systemd-analyze plot` pour récupérer une diagramme du boot, au format SVG
* il est possible de rediriger l'output de cette commande pour créer un fichier `.svg`
  * un `.svg` ça peut se lire avec un navigateur
* déterminer les 3 **services** les plus lents à démarrer

## 2. Gestion de l'heure

🌞 Utilisez la commande `timedatectl`
* déterminer votre fuseau horaire
* déterminer si vous êtes synchronisés avec un serveur NTP
* changer le fuseau horaire

## 3. Gestion des noms et de la résolution de noms

🌞 Utilisez `hostnamectl`
* déterminer votre hostname actuel
* changer votre hostname

# Structure du dépôt attendu

```bash
[it4@nowhere]$ tree tp3/
tp3/
├── README.md
├── scripts/
├── systemd/
│   ├── conf/
│   └── units/
└── Vagrantfile
```

* `scripts/` contient (si besoin) les scripts lancés par le Vagrantfile au boot des VMs
* `conf/` contient (si besoin) les fichiers de configuration relatifs à systemd
* `units/` contient les fichiers d'unités systemd
