# TP4 : Multi-node deployment

<!-- vim-markdown-toc GitLab -->

* [1. Repackaging](#1-repackaging)
* [2. Hosts](#2-hosts)

<!-- vim-markdown-toc -->

# 1. Repackaging

Le répertoire [base/](./base) est dédié au repackaging.

Instructions pour repackager :

```bash
$ cd base
$ vagrant up
$ vagrant package --output b2_tp4.box
$ vagrant box add b2_tp4 b2_tp4.box
```

# 2. Hosts

J'ai volontairement choisi trois sous-réseaux possédant des adresses privées TRES différentes, pour que l'utilisation de l'une ou de l'autre vous saute aux yeux.

> Attention ! Le `Vagrantfile` ne marchera chez vous que si ces réseaux ne sont pas déjà utilisés sur votre machine.

Networks :

| Network | Address            |
|---------|--------------------|
| App     | `192.168.117.0/24` |
| Backup  | `10.10.10.0/24`    |
| Front   | `172.20.20.0/24`   |

Hosts :

| Node                | Net App          | Net Backup    | Net Front      |
|---------------------|------------------|---------------|----------------|
| `gitea-node.tp4.b2` | `192.168.117.11` | `10.10.10.11` | `172.20.20.11` |
| `db-node.tp4.b2`    | `192.168.117.11` | `10.10.10.11` | X              |
| `nginx-node.tp4.b2` | `192.168.117.11` | `10.10.10.11` | `172.20.20.11` |
| `nfs-node.tp4.b2`   | X                | `10.10.10.11` | X              |

Schéma :

TODO : backup qui peut pas backup /srv/site1 et ste2 ça existe pas
